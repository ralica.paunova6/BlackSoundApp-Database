package blacksoundapp_database.repository;


import java.util.List;

import blacksoundapp_database.entities.BaseEntity;

public interface BaseRepository<T extends BaseEntity> {
	
	
	T getItem(int id);
	List<T> getAll();
	void add(T item);
	void update(T item);
	void delete(int id);
	
}
