package blacksoundapp_database.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import blacksoundapp_database.entities.BaseEntity;

public class BaseRepositoryImpl<T extends BaseEntity> implements BaseRepository<T>
{
	private EntityManagerFactory entityManagerFactory =  Persistence.createEntityManagerFactory("blacksound_db");
    protected EntityManager entityManager = entityManagerFactory.createEntityManager();
    private final Class<T> type;
     
    public BaseRepositoryImpl(Class<T> type) {
    	this.type = type;
    	
    }
	
    public T getItem(int id) {
		entityManager.getTransaction().begin();
		try {
			T item = type.newInstance();
			item = entityManager.find(type, id);
			entityManager.getTransaction().commit();
			return item;
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<T> getAll() {
		List<T> items =  new ArrayList<T>();
		
		entityManager.getTransaction().begin();
		items = entityManager.createQuery("SELECT t FROM "+type.getName()+" t").getResultList();
		entityManager.getTransaction().commit();
			
		return items;
	
	}

	public void add(T item) {
		entityManager.getTransaction().begin();
		entityManager.persist(item);
		entityManager.getTransaction().commit();
		
	}

	public void update(T item) {
		entityManager.getTransaction().begin();
		entityManager.merge(item);
		entityManager.getTransaction().commit();
		
	}

	public void delete(int id) {
		entityManager.getTransaction().begin();
		try {
			T item = type.newInstance();
			item = entityManager.find(type, id);
			entityManager.remove(item);
			entityManager.getTransaction().commit();
		} catch (InstantiationException e) {	
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		
		
	}
	
}
