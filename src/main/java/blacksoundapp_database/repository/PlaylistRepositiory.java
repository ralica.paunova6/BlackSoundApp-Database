package blacksoundapp_database.repository;

import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import blacksoundapp_database.entities.Playlist;
import blacksoundapp_database.entities.Song;
import blacksoundapp_database.entities.User;

public class PlaylistRepositiory extends BaseRepositoryImpl<Playlist>{

	public PlaylistRepositiory(Class<Playlist> type) {
		super(type);
		// TODO Auto-generated constructor stub
	}
	
	public List<Playlist> getUserAllPlaylists(int userId){
		List<Playlist> userPlaylists = entityManager.createQuery("SELECT p FROM Playlist p", Playlist.class).getResultList()
				.stream().filter(p -> p.getUser().getId() == userId).collect(Collectors.toList());
		return userPlaylists;
	}
	
	public List<Playlist> getAllPublicPlaylists(){
		List<Playlist> publicPlaylists = entityManager.createQuery("SELECT p FROM Playlist p", Playlist.class).getResultList()
				.stream().filter(p -> p.isPublic()==true).collect(Collectors.toList());
		return publicPlaylists;
	}
	
	public Set<Playlist> getSharedPlaylists(int userId){
		UserRepository userRepository = new UserRepository(User.class);
		User user = new User();
		user = userRepository.getItem(userId);
		Set<Playlist> sharedPlaylists = user.getSharedPlaylists();
		return sharedPlaylists;
		/*Set<Playlist> sharedPlaylists = entityManager.createQuery("SELECT p FROM Playlist p", Playlist.class).getResultList()
				.stream().filter(p -> ).collect(Collectors.toSet());
		return sharedPlaylists;*/
	}
	
	
}
