package blacksoundapp_database.repository;

import java.util.List;
import java.util.NoSuchElementException;
import blacksoundapp_database.entities.User;


public class UserRepository extends BaseRepositoryImpl<User>{

	public UserRepository(Class<User> type) {
		super(type);
	}
	
	public User getByEmailPassword(String email, String password) {
		User user = new User();
		try {
			user = entityManager.createQuery("SELECT u FROM User u",User.class).getResultList().stream().
					filter(u -> u.getEmail().equals(email) & u.getPassword().equals(password)).findFirst().get();
					
		} catch (NoSuchElementException e) {
			return null;
		}
		return user;
	}
	
	
}
