package blacksoundapp_database.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;



@Entity
@Table(name = "songs")
public class Song extends BaseEntity {

	@Column(name="title")
	private String title;
	@Column(name="artist_name")
	private String artistName;
	@Column(name="year")
	private int year;
	@ManyToMany(mappedBy="songs")
	private Set<Playlist> playlists = new HashSet<Playlist>();
	

	public Song() {
		// TODO Auto-generated constructor stub
	}
	
	public Song(String title, String artistName, int year) {
		this.title = title;
		this.artistName = artistName;
		this.year = year;	
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getArtistName() {
		return artistName;
	}
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
	
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	
	public Set<Playlist> getPlaylists() {
		return playlists;
	}
	public void setPlaylists(Set<Playlist> playlists) {
		this.playlists = playlists;
	}
	
	public void addPlaylist(Playlist playlist) {
		playlists.add(playlist);
		playlist.addSong(this);;
	}
	
	public void removePlaylist(Playlist playlist) {
		playlists.remove(playlist);
		playlist.addSong(null);
	}
}
