package blacksoundapp_database.entities;

import java.util.HashSet;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.transaction.UserTransaction;
import javax.persistence.JoinColumn;



@Entity
@Table(name="playlists")
public class Playlist extends BaseEntity {
	
	@Column(name="name")
	private String name;
	@Column(name="description")
	private String description;
	@ManyToMany(cascade = {CascadeType.PERSIST,CascadeType.MERGE})
	@JoinTable(name = "playlist_song",joinColumns=@JoinColumn(name="playlist_id"), inverseJoinColumns = @JoinColumn(name ="song_id"))
	private Set<Song> songs = new HashSet<Song>();
	@Column(name="is_public")
	private boolean isPublic;
	@ManyToOne(fetch = FetchType.LAZY)
	private User user;
	@ManyToMany(cascade = {CascadeType.PERSIST,CascadeType.MERGE})
	@JoinTable(name = "playlist_user",joinColumns=@JoinColumn(name="playlist_id"), inverseJoinColumns = @JoinColumn(name ="user_id"))
	private Set<User> sharedUsers = new HashSet<User>();
	
	public Playlist() {
		// TODO Auto-generated constructor stub
	}
	
	public Playlist(String name, String description, boolean isPublic) {
		this.name = name;
		this.description = description;
		this.isPublic = isPublic;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public Set<Song> getSongs() {
		return songs;
	}
	public void setSongs(Set<Song> songs) {
		this.songs = songs;
	}
	
	public Set<User> getSharedUsers() {
		return sharedUsers;
	}
	public void setSharedUsers(Set<User> sharedUsers) {
		this.sharedUsers = sharedUsers;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public boolean isPublic() {
		return isPublic;
	}
	public void setPublic(boolean isPublic) {
		this.isPublic = isPublic;
	}
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	public void addSong(Song song) {
		songs.add(song);
		song.addPlaylist(this);
	}
	
	public void removeSong(Song song) {
		songs.remove(song);
		song.addPlaylist(null);
	}
	
	public void addSharedUser(User user) {
		sharedUsers.add(user);
		user.addSharedPlaylist(this);
	}
	
	/*public void removeSharedUser(User user) {
		sharedUsers.remove(user);
		user.addSharedPlaylist(null);
	}*/
}
