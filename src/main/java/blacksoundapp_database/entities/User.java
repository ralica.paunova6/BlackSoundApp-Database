package blacksoundapp_database.entities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;





@Entity
@Table(name = "users", uniqueConstraints=@UniqueConstraint(columnNames={"email"}))
public class User extends BaseEntity{
	
	@Column(name ="email")
	private String email;
	@Column(name ="password")
	private String password;
	@Column(name ="display_name")
	private String displayName;
	@Column(name = "is_admin")
	private boolean isAdmin;
	@OneToMany(mappedBy ="user",cascade = CascadeType.ALL,orphanRemoval = true)
	private List<Playlist> playlists = new ArrayList<Playlist>();
	@ManyToMany(mappedBy = "sharedUsers")
	private Set<Playlist> sharedPlaylists = new HashSet<Playlist>();
	
	public User() {

	}
	
	public User(String email, String password, String displayName, boolean isAdmin) {
		this.email = email;
		this.password = password;
		this.displayName = displayName;
		this.isAdmin = isAdmin;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
	
	public void setSharedPlaylists(Set<Playlist> sharedPlaylists) {
		this.sharedPlaylists = sharedPlaylists;
	}
	
	public Set<Playlist> getSharedPlaylists(){
		return sharedPlaylists;
	}
	
	public void setPlaylists(List<Playlist> playlists){
		this.playlists = playlists;
	}
	
	public List<Playlist> getPlaylists(){
		return playlists;
	}
	
	public void addPlaylist(Playlist playlist) {
		playlists.add(playlist);
		playlist.setUser(this);
	}
	
	public void removePlaylist(Playlist playlist) {
		playlists.remove(playlist);
		playlist.setUser(null);
	}
	
	public void addSharedPlaylist(Playlist playlist) {
		sharedPlaylists.add(playlist);
		playlist.addSharedUser(this);
	}
	/*public void removeSharedPlaylist(Playlist playlist) {
		sharedPlaylists.remove(playlist);
		playlist.addSharedUser(null);
	}*/
}
