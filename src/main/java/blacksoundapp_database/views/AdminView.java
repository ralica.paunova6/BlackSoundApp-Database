package blacksoundapp_database.views;

import java.util.Scanner;

import blacksoundapp_database.entities.Song;
import blacksoundapp_database.entities.User;
import blacksoundapp_database.services.AuthService;


public class AdminView {
	Scanner scanner = new Scanner(System.in);
	
	public void  run() {
		
		if (AuthService.getInstance().getLoggedUser().isAdmin()) {
			int choice = getMenuChoice();
				switch (choice) {
				case 1:
					BaseView<User>  userView = new UserView();
					userView.run();
					break;
				case 2:
					BaseView<Song> songView =  new SongView();
					songView.run();
					break;
				default:
					getMenuChoice();
					break;
				}
		}
	}
	
	private int getMenuChoice() {
		int choice = 0;
		while (true) {
			System.out.println("Select menu");
			System.out.println("1. Users");
			System.out.println("2. Songs");
			String input = scanner.next();
			if (!input.isEmpty()&&input.matches("\\d")) {
				return choice=  Integer.parseInt(input);
			}else {
				System.out.println("Invalid input! Try again.\n");
			}
		}
	}
}
