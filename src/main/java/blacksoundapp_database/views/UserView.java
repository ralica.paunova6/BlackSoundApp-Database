package blacksoundapp_database.views;


import blacksoundapp_database.entities.User;
import blacksoundapp_database.repository.BaseRepository;
import blacksoundapp_database.repository.UserRepository;

public class UserView extends BaseView<User>{

	UserRepository userRepository = new UserRepository(User.class);
	@Override
	public BaseRepository<User> returnRepo() {
		this.entityType = User.class;
		return userRepository;
	}

	@Override
	public void initAdd(User item) {
		
		while (true) {
			System.out.println("Add data for the new user");
			System.out.println("Set email: ");
			item.setEmail(scanner.next());
			//System.out.println("Input email!");continue;
			System.out.println("Set password: ");
			item.setPassword(scanner.next());
			//System.out.println("Input password!");continue;
			System.out.println("Set name: ");
			item.setDisplayName(scanner.next());
			//System.out.println("Input name!");continue;
			System.out.println("Is Admin? (y/n)");
			if (scanner.next().equals("y")) {
				item.setAdmin(true); 
				break;
			}else if (scanner.next().equals("n")) {
				item.setAdmin(false); 
				break;
			}else {
				System.out.println("Invalid input! Try again.");
			}
		}
	}

	@Override
	public void initList() {
		for (User user : userRepository.getAll()) {
			System.out.println(user.getId()+". "+user.getDisplayName()+" Email: "+user.getEmail()+" "+(user.isAdmin()==true?"Admin":"User"));
		}
		
	}

	@Override
	public User initEdit(User item) {
		System.out.println("Edit user. Select user do you want to edit.");
		
		initList();
		System.out.println("\nUser ID:");
		int userId = getValidNumberInput();
		
		item =userRepository.getItem(userId);
		
		while (true) {
			System.out.println("Edit User");
			System.out.println("Email: ");
			item.setEmail(scanner.next());
			System.out.println("Password: ");
			item.setPassword(scanner.next());
			System.out.println("Name: ");
			item.setDisplayName(scanner.next());
			System.out.println("Is Admin? (y/n)");
			if (scanner.next().equals("y")) {
				item.setAdmin(true);
				return item;
			}else if (scanner.next().equals("n")) {
				item.setAdmin(false);
				return item;
			}else {
				System.out.println("Invalid input! Try again.");
			}
		}
	}

	@Override
	public void initDelete() {
		initList();
		System.out.println("Delete User:");
		System.out.println("User ID:");
		
	}

	@Override
	public void initBack() {
		AdminView adminView = new AdminView();
		adminView.run();
		
	}

	@Override
	public MenuItems switchCustomMenuItems(int choice) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void switchCustomRenderedMenuItems(MenuItems item) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void listCustomsMenuItems() {
		// TODO Auto-generated method stub
		
	}
	
}
