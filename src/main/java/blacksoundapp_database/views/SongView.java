package blacksoundapp_database.views;

import blacksoundapp_database.entities.Song;
import blacksoundapp_database.repository.BaseRepository;
import blacksoundapp_database.repository.SongRepository;

public class SongView extends BaseView<Song>{

	SongRepository songRepository = new SongRepository(Song.class);
	@Override
	public BaseRepository<Song> returnRepo() {
		this.entityType = Song.class;
		return songRepository;
	}

	@Override
	public void initAdd(Song item) {
		while (true) {
			System.out.println("Add data for the new song");
			System.out.println("Set title: ");
			item.setTitle(scanner.next());;
			System.out.println("Set artist(s): ");
			item.setArtistName(scanner.next());;
			System.out.println("Set year: ");
			String input = scanner.next();
			if (input.matches("\\d{4}")) {
				item.setYear(Integer.parseInt(input)); 
				break;
			}else{
				System.out.println("Input valid year! try again.\n"); 
			}
		}
		
	}

	@Override
	public void initList() {
		for (Song song : songRepository.getAll()) {
			System.out.println(song.getId()+". Title: "+song.getTitle()+" Artist(s): "+song.getArtistName()+" Year:"+song.getYear());
		}
		
	}

	@Override
	public Song initEdit(Song item) {
		System.out.println("Edit song. Select song do you want to edit.");
		initList();
		System.out.println("Song ID:");
		int songId = getValidNumberInput();
		
		item =songRepository.getItem(songId);
		item.setId(songId);
		while (true) {
			System.out.println("Edit Song");
			System.out.println("Title: ");
			item.setTitle(scanner.next());
			System.out.println("Artist(s): ");
			item.setArtistName(scanner.next());
			System.out.println("Year: ");
			String input = scanner.next();
			if (input.matches("\\d{4}")) {
				item.setYear(Integer.parseInt(input)); 
				return item;
			}else{
				System.out.println("Input valid year! try again.\n"); 
			}	
	}
	}

	@Override
	public void initDelete() {
		initList();
		System.out.println("\nDelete Song:");
		System.out.println("Song ID:");
		
	}

	@Override
	public void initBack() {
		AdminView adminView = new AdminView();
		adminView.run();		
		
	}

	@Override
	public MenuItems switchCustomMenuItems(int choice) {
		return null;
	}

	@Override
	public void switchCustomRenderedMenuItems(MenuItems item) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void listCustomsMenuItems() {
		// TODO Auto-generated method stub
		
	}

}
