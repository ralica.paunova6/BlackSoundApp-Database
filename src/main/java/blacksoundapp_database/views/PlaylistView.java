package blacksoundapp_database.views;


import java.util.HashSet;
import java.util.Set;

import blacksoundapp_database.entities.Playlist;
import blacksoundapp_database.entities.Song;
import blacksoundapp_database.entities.User;
import blacksoundapp_database.repository.BaseRepository;
import blacksoundapp_database.repository.PlaylistRepositiory;
import blacksoundapp_database.repository.SongRepository;
import blacksoundapp_database.repository.UserRepository;
import blacksoundapp_database.services.AuthService;

public class PlaylistView extends BaseView<Playlist>{

	PlaylistRepositiory playlistRepo = new PlaylistRepositiory(Playlist.class);
	@Override
	public BaseRepository<Playlist> returnRepo() {
		this.entityType = Playlist.class;
		return playlistRepo;
	}

	@Override
	public void initAdd(Playlist playlist) {
		while (true) {
			System.out.println("\nAdd data for the new playlist");
			System.out.println("Set name: ");
			playlist.setName(scanner.next());;
			System.out.println("Set description: ");
			playlist.setDescription(scanner.next());
			playlist.setUser(AuthService.getInstance().getLoggedUser());
			System.out.println("Is Public? (y/n)");
			if (scanner.next().equals("y")) {
				playlist.setPublic(true);break;
			}else if (scanner.next().equals("n")) {
				playlist.setPublic(false);break;
			}else {
				System.out.println("Invalid input! Try again.");
			}
		}
		
	}

	@Override
	public void initList() {
		listUserPlaylists();
		
		listPublicPlaylists();
		
		listSharedPlaylists();
		
	}

	private void listUserPlaylists() {
		System.out.println("My playlists:");
		for (Playlist playlist : playlistRepo.getUserAllPlaylists(AuthService.getInstance().getLoggedUser().getId())) {
			System.out.println(playlist.getId()+". Name: "+playlist.getName()+" Description: "+playlist.getDescription());
		}
		
	}
	
	private void listPublicPlaylists() {
		System.out.println("\nPublic playlists:");
		for (Playlist playlist : playlistRepo.getAllPublicPlaylists()) {
			System.out.println(playlist.getId()+". Name: "+playlist.getName()+" Description: "+playlist.getDescription()+" Owner: "+playlist.getUser().getDisplayName());
		}
	}
	
	private void listSharedPlaylists() {
		System.out.println("\nShared with me:");
		Set<Playlist> sharedPlaylists = playlistRepo.getSharedPlaylists(AuthService.getInstance().getLoggedUser().getId());
		
		if (!sharedPlaylists.isEmpty()) {
			/*for (Playlist playlist:sharedPlaylists) {
				System.out.println(playlist.getId()+". Name: "+playlist.getName()+" Description: "+playlist.getDescription());
			}*/
			System.out.println("fghgsfghshsf");
		}else {
			System.out.println("There are no shared playlists!\n");
		}
	}
	
	@Override
	public Playlist initEdit(Playlist playlist) {
		System.out.println("\nEdit playlist. Select playlist do you want to edit.");
		listUserPlaylists();
		
		System.out.println("\nPlaylist ID:");
		int playlistId = getValidNumberInput();
		playlist =playlistRepo.getItem(playlistId);
		
		while (true) {
			System.out.println("\nEdit Playlist");
			System.out.println("Playlist name: ");
			playlist.setName(scanner.next());
			System.out.println("Description: ");
			playlist.setDescription(scanner.next());
			System.out.println("Is Public? (y/n): ");
			if (scanner.next().equals("y")) {
				playlist.setPublic(true);
				return playlist;
			}else if (scanner.next().equals("n")) {
				playlist.setPublic(false);
				return playlist;
			}else {
				System.out.println("Invalid input! Try again.");
			}
		}
	}

	@Override
	public void initDelete() {
		listUserPlaylists();
		
		System.out.println("\nDelete Playlist:");
		System.out.println("Playlist ID:");
		//int playlistId =  getValidNumberInput();
		//playlistRepo.deleteCascade(playlistId);
	}

	@Override
	public void initBack() {
		
	}

	@Override
	public MenuItems switchCustomMenuItems(int choice) {
		switch (choice) {
		case 8:
			return MenuItems.CUSTOM;
		default:
			return null;
		}
	}

	@Override
	public void switchCustomRenderedMenuItems(MenuItems item) {
		switch (item) {
		case CUSTOM:
			initOptionsForPlaylist();
			break;

		default:
			break;
		}
	}

	@Override
	public void listCustomsMenuItems() {
		System.out.println("8.Options");
	}
	
public void initOptionsForPlaylist() {
		
		System.out.println("Select option for the playlist:");
		System.out.println("1.View songs from playlist");
		System.out.println("2.Add song to the playlist");
		System.out.println("3.Delete song from playlist");
		System.out.println("4.Share playlist with other user");
		System.out.println("5.Back");
		int choice = getValidNumberInput();
		
		switch (choice) {
		case 1: 
			listSongsFromPlaylist();
			break;
		case 2: 
			setSongToPlaylist();
			break;
		case 3:
			deleteSongFromPlaylist();
			break;
		case 4:
			sharePlaylist();
		case 5:
			getBack();
			break;

		default:
			break;
		}
	}

	public void getBack() {
		run();
	}
	
	public void listSongsFromPlaylist() {
			
		listUserPlaylists();
		listPublicPlaylists();
		listSharedPlaylists();
			
		//method start
		System.out.println("To list songs from specific playlist, select playlist ID from the list:");
		int playlistId = getValidNumberInput();
			
		Playlist playlist =  playlistRepo.getItem(playlistId); 
		System.out.println("\nThe chosen playlist is: ");
		System.out.println(playlist.getName()+" "+playlist.getDescription());
		
		Set<Song> songs = playlist.getSongs();
		if (songs.isEmpty()) {
			System.out.println("There are no songs in this playlist!");
		}else {
			for (Song song : songs) {
				System.out.println("\tName: " +song.getTitle() + " Artis(s): "+song.getArtistName()+" Year: "+song.getYear());
			}
		}
			System.out.println("");
	}
	
	public void setSongToPlaylist() {
		SongRepository songRepo =  new SongRepository(Song.class);
		
		listUserPlaylists();
		listSharedPlaylists();
		
		System.out.println("To add a song to specific playlist, select playlist ID from the list:");
		int playlistId = getValidNumberInput();
		
		Playlist playlist =  playlistRepo.getItem(playlistId);
		/*System.out.println("\nThe chosen playlist is: ");
		System.out.println(playlist.getName()+" "+playlist.getDescription()); */
		
		System.out.println("List of songs:");
		for (Song song : songRepo.getAll()) {
			System.out.println(song.getId()+". Title: "+song.getTitle()+" Artist(s): "+song.getArtistName()+" Year:"+song.getYear());
		}
		System.out.println("\nChoose a song from the list above:");
		int choice = getValidNumberInput();
		
		Song song = songRepo.getItem(choice);
		
		Set<Song> songs = playlist.getSongs();
		Set<Playlist> playlists = new HashSet<>();
		songs.add(song);
		playlists.add(playlist);
		song.setPlaylists(playlists);
		playlist.setSongs(songs);
		//playlist.addSong(song);
		playlistRepo.update(playlist);
		
	}

	public void deleteSongFromPlaylist() {
		SongRepository songRepo =  new SongRepository(Song.class);
		
		listUserPlaylists();
		listSharedPlaylists();
		
		System.out.println("To delete songs from specific playlist, select playlist ID from the list:");
		int playlistId = getValidNumberInput();
		
		Playlist playlist = playlistRepo.getItem(playlistId); 
		System.out.println("\nThe chosen playlist is: ");
		System.out.println(playlist.getName()+" "+playlist.getDescription());
		
		Set<Song> songs = playlist.getSongs();
		for (Song song : songs) {
			System.out.println("\t"+song.getId()+". Name: " +song.getTitle() + " Artis(s): "+song.getArtistName()+" Year: "+song.getYear());
		}
		System.out.println("\nTo delete song from this playlist select song ID: ");
		int songId = getValidNumberInput();
		
		Song song = songRepo.getItem(songId);
		Set<Playlist> playlists = song.getPlaylists();
		song.getPlaylists().remove(playlist);
		playlist.getSongs().remove(song);
		//playlists.remove(song);
		//songs.remove(playlist);
		//song.setPlaylists(playlists);
		//playlist.setSongs(songs);
		
		//playlist.addSong(song);
		songRepo.update(song);
		playlistRepo.update(playlist);
		
	}
	
	public void sharePlaylist() {
		System.out.println("Select playlist ID you want to share:\n ");
		
		listUserPlaylists();
		
		int playlistId = getValidNumberInput();
		
		System.out.println("\nSelect user, with whom you want to share selected playlist.");
		UserRepository userRepository = new UserRepository(User.class);
		for (User user : userRepository.getAll()) {
			if (user.getId()==AuthService.getInstance().getLoggedUser().getId()) {
				continue;
			}
			System.out.println(user.getId()+". "+user.getDisplayName()+" Email: "+user.getEmail()+" "+(user.isAdmin()==true?"Admin":"User"));
		}
		int userId =  getValidNumberInput();
		
		Playlist playlist = playlistRepo.getItem(playlistId);
	
		User  user = userRepository.getItem(userId);
		Set<User> users = new HashSet<>();
		Set<Playlist> playlists = user.getSharedPlaylists();
		users.add(user);
		playlists.add(playlist);
		user.setSharedPlaylists(playlists);
		playlist.setSharedUsers(users);
		
		playlistRepo.update(playlist);
	}
}
