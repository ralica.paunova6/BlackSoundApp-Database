package blacksoundapp_database.services;

import blacksoundapp_database.entities.User;
import blacksoundapp_database.repository.UserRepository;

public class AuthService {
	
	private static AuthService instance =  null;
	private User loggedUser =  null;
	
	private AuthService(){
		
	}
	
	public static AuthService getInstance() {
		
		if (AuthService.instance==null) {
			return AuthService.instance = new AuthService();
		}
		return AuthService.instance;
	}
	public User getLoggedUser() {
		return loggedUser;
	}

	public void setLoggedUser(String email, String password) {
		UserRepository userRepo=  new UserRepository(User.class);
		this.loggedUser = userRepo.getByEmailPassword(email, password);
	}
}
